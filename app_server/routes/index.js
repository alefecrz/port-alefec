const express = require('express');
const router = express.Router();
const ctrlProjects = require('../controllers/project');
const ctrlOthers = require('../controllers/others');
const ctrlEmail = require('../controllers/email');

/* GET home page. */
router.get('/', ctrlProjects.home);
router.get('/project', ctrlProjects.projectDetail);
router.get('/project/review/new', ctrlProjects.projectReview);

router.get('/about', ctrlOthers.about);
router.get('/contact', ctrlOthers.contact);

router.post('/send-email', ctrlEmail.send);

module.exports = router;
